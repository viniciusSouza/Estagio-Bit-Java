package com.company.lista7A;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            //Scanner scan = new Scanner(System.in);
            //System.out.println("Informe o caminho do arquivo de tempos: ");
            //String caminho = scan.nextLine();
            String caminho = "D:\\Workspace\\JavaBasico\\arquivosTeste\\tempos.txt";
            String[] arquivoCorrida = ArquivoUtil.lerArquivoEmArrayString(caminho);
            Corrida corrida = new Corrida();
            ArquivoUtil.lerQntidadePilotosEvoltas(arquivoCorrida, corrida);
            ArquivoUtil.lerInformacoesDeCadaPiloto(arquivoCorrida, corrida);
            System.out.println("-------Dados da corrida-------");
            corrida.getDadosCorrida();
            //System.out.println();
            //corrida.getMelhorDaProva();
            ArquivoUtil.escreverArquivoDeSaida(corrida);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
