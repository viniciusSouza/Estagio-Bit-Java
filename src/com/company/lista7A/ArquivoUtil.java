package com.company.lista7A;

import java.io.*;

public class ArquivoUtil {

    public static int posicaoInicialDePilotos;

    public static String[] lerArquivoEmArrayString(String caminho) throws IOException {
        try {
            FileReader arquivo = new FileReader(caminho);
            BufferedReader lerArquivo = new BufferedReader(arquivo);
            StringBuilder stringBuilder = new StringBuilder();
            String linha = lerArquivo.readLine();
            while (linha != null) {
                stringBuilder.append(linha + " ");
                linha = lerArquivo.readLine();
            }
            arquivo.close();
            lerArquivo.close();
            return stringBuilder.toString().split(" ");
        } catch (IOException e) {
            throw new IOException("Arquivo não encontrado.");
        }
    }

    public static void lerQntidadePilotosEvoltas(String[] arquivo, Corrida corrida) {
        for (int i = 0; i < arquivo.length; i++) {
            if(arquivo[i].equalsIgnoreCase("PILOTOS:")) {
                corrida.numeroPilotos = Integer.parseInt(arquivo[i+1]);
            } else if (arquivo[i].equalsIgnoreCase("VOLTAS:")) {
                corrida.numeroVoltas = Integer.parseInt(arquivo[i+1]);
                posicaoInicialDePilotos = i+2;
            }
        }
    }

    public static void lerInformacoesDeCadaPiloto(String[] arquivo, Corrida corrida) {
        int indiceInicial = posicaoInicialDePilotos;
        int contadorTempos = 0;
        boolean sobrenome = false;
        for (int i = 0; i < corrida.numeroPilotos; i++) {
            Piloto piloto = new Piloto(corrida.numeroVoltas);
            for (int j = 0; j <= corrida.numeroVoltas + 1; j++) {
                if (!arquivo[indiceInicial].contains(".")) {
                    if (!sobrenome) {
                        piloto.setNome(arquivo[indiceInicial]);
                        sobrenome = true;
                    } else {
                        piloto.setSobrenome(arquivo[indiceInicial]);
                        sobrenome = false;
                    }
                } else {
                    piloto.setTempos(contadorTempos, Double.parseDouble(arquivo[indiceInicial]));
                    contadorTempos++;
                }
                indiceInicial++;
                if (contadorTempos == corrida.numeroVoltas) {
                    contadorTempos = 0;
                }
            }
            corrida.listaDePilotos.add(piloto);
        }
    }

    public static void escreverArquivoDeSaida(Corrida corrida) {
        try {
            File arquivo = new File("saida.txt");
            PrintWriter writer = new PrintWriter(arquivo);
            writer.println(corrida.getMelhorDaProva());
            for (Piloto p : corrida.listaDePilotos) {
                writer.println("--------------------------------------");
                writer.println("Dados " + p.getNome() + " " + p.getSobrenome() + ": ");
                writer.println("Melhor tempo: " + p.getMelhorTempoDeProva() + " na volta: " + p.getMelhorVolta());
                writer.println("Pior tempo: " + p.getPiorTempoDeProva() + " na volta: " + p.getPiorVolta());
                writer.println("Tempo médio:  " + p.getTempoMedioPorVolta());
                writer.println("--------------------------------------");
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}

