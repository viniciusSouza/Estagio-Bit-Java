package com.company.lista7A;

import java.util.ArrayList;
import java.util.List;

public class Corrida {
    protected int numeroVoltas;
    protected int numeroPilotos;
    protected List<Piloto> listaDePilotos = new ArrayList<>();

    public List<Piloto> getListaDePilotos() {
        return listaDePilotos;
    }

    public int getNumeroVoltas() {
        return numeroVoltas;
    }

    public int getNumeroPilotos() {
        return numeroPilotos;
    }

    public void getDadosCorrida() {
        System.out.println("Numero de pilotos: " + numeroPilotos);
        System.out.println("Numero de voltas: " + numeroVoltas);
        for (Piloto p : this.listaDePilotos) {
            System.out.println("Piloto: " + p.getNome() + " " + p.getSobrenome());
            double[] tempos = p.getTempos();
            for (int i = 0; i < p.getTempos().length; i++) {
                System.out.println("Tempo " + (i+1) + " volta: " + tempos[i]);
            }
        }
    }

    public String getMelhorDaProva() {
        Piloto vencedor = listaDePilotos.get(0);
        for (Piloto p : listaDePilotos) {
            if (p.getMelhorTempoDeProva() > vencedor.getMelhorTempoDeProva()) {
                vencedor = p;
            }
        }
        return "O vencedor da prova foi: " + vencedor.getNome() + " " +
                vencedor.getSobrenome() + " e sua melhor volta foi a volta " +
                vencedor.getMelhorVolta() + " com o tempo de: " + vencedor.getMelhorTempoDeProva();
    }

}
