package com.company.lista7A;

public class Piloto {
    String nome;
    String sobrenome;
    double[] tempos;

    public Piloto(int numVoltas) {
        this.tempos = new double[numVoltas];
    }

    public Piloto() {
    }

    public String getNome() {
        return nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public double[] getTempos() {
        return tempos;
    }

    public void setTempos(int index, double tempo) {
        this.tempos[index] = tempo;
    }

    public double getMelhorTempoDeProva() {
        double melhorTempo = tempos[0];
        for (double tempo : tempos) {
            if (tempo > melhorTempo) {
                melhorTempo = tempo;
            }
        }
        return melhorTempo;
    }

    public double getPiorTempoDeProva() {
        double piorTempo = tempos[0];
        for (double tempo : tempos) {
            if (tempo < piorTempo) {
                piorTempo = tempo;
            }
        }
        return piorTempo;
    }

    public int getMelhorVolta() {
        int melhorVolta = 0;
        for (int i = 0; i < tempos.length; i++) {
            if (tempos[i] == this.getMelhorTempoDeProva()) {
                melhorVolta = i;
            }
        }
        return melhorVolta+1;
    }

    public int getPiorVolta() {
        int piorVolta = 0;
        for (int i = 0; i < tempos.length; i++) {
            if (tempos[i] == this.getPiorTempoDeProva()) {
                piorVolta = i;
            }
        }
        return piorVolta+1;
    }

    public double getTempoMedioPorVolta() {
        double tempoTotal = 0;
        for (int i = 0; i < tempos.length; i++) {
            tempoTotal += tempos[i];
        }
        return tempoTotal/tempos.length;
    }

}
