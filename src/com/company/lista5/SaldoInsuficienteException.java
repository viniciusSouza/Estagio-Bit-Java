package com.company.lista5;

public class SaldoInsuficienteException extends RuntimeException {
    public SaldoInsuficienteException(String mensagem, double valor) {
        super(mensagem + valor);
    }
}
