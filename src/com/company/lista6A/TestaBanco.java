package com.company.lista6A;

import com.company.lista4.ContaCorrente;
import com.company.lista4.ContaPoupanca;

public class TestaBanco {
    public static void main(String[] args) {
        Banco banco = new Banco("Centro", 123);
        ContaCorrente cc = new ContaCorrente(123,500.,2., "Vinicius");
        ContaPoupanca cp = new ContaPoupanca(1234, 500., 100., "Carlos");
        banco.inserirConta(cc);
        banco.inserirConta(cp);
        for (int i = 1; i <= 15; i++) {
            ContaCorrente c = new ContaCorrente(i,i*100.,2., "Joao" + i);
            banco.inserirConta(c);
        }
        banco.mostrarContas();
        int numeroConta = 3;
        if(banco.contemConta(numeroConta)) {
            System.out.println("Conta " + numeroConta + " encontrada.");
        } else {
            System.out.println("Conta " + numeroConta + " não encontrada.");
        }

        numeroConta = 10;
        if(banco.contemConta(numeroConta)) {
            System.out.println("Conta " + numeroConta + " encontrada.");
        } else {
            System.out.println("Conta " + numeroConta + " não encontrada.");
        }

        banco.aumentarCapacidadeBanco();
        for (int i = 1; i <= 15; i++) {
            ContaCorrente c = new ContaCorrente(i+10,i*100.,2., "Maria" + i);
            banco.inserirConta(c);
        }
        banco.mostrarContas();
    }
}
