package com.company.lista6A;

import com.company.lista2.Fibonacci;

public class FibonacciComArray {
    private long[] cache;

    public FibonacciComArray(int numero) {
        this.cache = new long[numero];
        for (int i = 0; i < numero; i++) {
            cache[i] = 0;
        }
    }

    public long calcularFibonacci(int numero) {
        if (numero <= 1) {
            this.cache[numero] = numero;
            return numero;
        }
        if (this.cache[numero] == 0) {
            return this.cache[numero] = calcularFibonacci(numero - 1) + calcularFibonacci(numero - 2);
        } else {
            return this.cache[numero];
        }
    }

    public static void main(String[] args) {
        int numero = 45;
        FibonacciComArray fiboArray = new FibonacciComArray(numero);
        long inicio = System.currentTimeMillis();
        for (int i = 0; i < numero; i++) {
            System.out.print(fiboArray.calcularFibonacci(i) + " ");
        }
        long tempo = System.currentTimeMillis() - inicio;
        System.out.println();
        System.out.println("Tempo da versão com array: " + tempo);

        inicio = System.currentTimeMillis();
        for (int i = 1; i < numero; i++) {
            int fibonacci = Fibonacci.fibonacciRecursivo(i);
            System.out.print(fibonacci + " ");
        }
        tempo = System.currentTimeMillis() - inicio;
        System.out.println();
        System.out.println("Tempo da versão recursiva: " + tempo);
    }
}
