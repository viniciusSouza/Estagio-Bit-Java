package com.company.lista6A;

import com.company.lista4.ContaBancaria;

public class Banco {
    private static int contador = 0;
    private String nome;
    private int numero;
    private ContaBancaria[] contas;

    public Banco(String nome, int numero) {
        this.nome = nome;
        this.numero = numero;
        this.contas = new ContaBancaria[10];
    }

    public int getNumero() {
        return numero;
    }

    public String getNome() {
        return nome;
    }

    public ContaBancaria[] getContas() {
        return contas;
    }

    public void inserirConta(ContaBancaria contaBancaria) {
        if (contador >= this.contas.length) {
            System.out.println("Banco lotado!!");
        } else {
            this.contas[contador] = contaBancaria;
            contador++;
            System.out.println("Conta adicionada com sucesso.");
        }
    }

    public void mostrarContas() {
        for (ContaBancaria c : contas) {
            c.mostrarDados();
        }
    }

    public boolean contemConta(int numeroConta) {
        boolean verificador = false;
        for (ContaBancaria cb : this.contas) {
            if (cb.getNumeroConta() == numeroConta) {
                verificador = true;
            }
        }
        return verificador;
    }

    public void aumentarCapacidadeBanco() {
        ContaBancaria[] realocacao = new ContaBancaria[contador + 10];
        for (int i = 0; i < contas.length; i++) {
            realocacao[i] = contas[i];
        }
        this.contas = realocacao;
    }
}
