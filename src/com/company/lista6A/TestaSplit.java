package com.company.lista6A;

public class TestaSplit {
    public static void main(String[] args) {
        String fraseOriginal = "Socorram-me, subi no ônibus em Marrocos";
        String[] palavras = fraseOriginal.split(" ");
        for (int i = palavras.length - 1; i >= 0; i--) {
            System.out.print(palavras[i] + " ");
        }
    }
}
