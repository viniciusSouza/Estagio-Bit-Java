package com.company.lista6A;

import com.company.lista4.ContaBancaria;
import com.company.lista4.ContaCorrente;

public class TestaArrays {
    public static void main(String[] args) {
        ContaBancaria[] contas = new ContaBancaria[10];
        for (int i = 0; i < contas.length; i++) {
            ContaBancaria conta = new ContaCorrente(i,i*100.,2., "Joao" + i);
            contas[i] = conta;
        }
        double total = 0;
        for (ContaBancaria c : contas) {
            total += c.getSaldo();
        }
        System.out.println(total/contas.length);
    }
}
