package com.company.lista4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Banco implements Imprimivel{
    private List<ContaBancaria> contasBancarias = new ArrayList<>();
    private Map<String,ContaBancaria> contaBancariasMap = new HashMap<>();

    public List<ContaBancaria> getContasBancarias() {
        return contasBancarias;
    }

    public boolean inserirConta(ContaBancaria contaBancaria) {
        try {
            if (contaBancaria != null) {
                this.contasBancarias.add(contaBancaria);
                this.contaBancariasMap.put(contaBancaria.titular, contaBancaria);
                System.out.println("Conta " + contaBancaria.numeroConta + " inserida com sucesso.");
                return true;
            } else {
                throw new IllegalArgumentException("Conta inválida. Inserção não realizada.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean removerConta(ContaBancaria contaBancaria) {
        try {
            if (contaBancaria != null) {
                this.contasBancarias.remove(contaBancaria);
                System.out.println("Conta " + contaBancaria.numeroConta + " removida com sucesso.");
                return true;
            } else {
                throw new IllegalArgumentException("Conta inexistente.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public ContaBancaria procurarConta(int numeroConta) {
        try {
            ContaBancaria contaBancaria = null;
            for (ContaBancaria c : contasBancarias) {
                if (c.numeroConta == numeroConta) {
                    contaBancaria = c;
                }
            }
            if (contaBancaria == null) {
                throw new IllegalArgumentException("Conta inexistente.");
            }
            return contaBancaria;
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public void mostrarDados() {
        for (ContaBancaria c : this.contasBancarias) {
            c.mostrarDados();
        }
    }

    public int totalDeContas() {
        return contasBancarias.size();
    }

    public ContaBancaria buscarContaPorTitular(String nome) {
        try {
            ContaBancaria contaBancaria = null;
            for (ContaBancaria cb : this.contasBancarias) {
                if (cb.titular.equals(nome)) {
                    contaBancaria = cb;
                }
            }
            if (contaBancaria == null) {
                throw new IllegalArgumentException("Conta não encontrada.");
            } else {
                return contaBancaria;
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public ContaBancaria buscarContaPorTitularNoMap(String nome) {
        try {
            if (this.contaBancariasMap.get(nome) != null) {
                return this.contaBancariasMap.get(nome);
            } else {
                throw new IllegalArgumentException("Conta não encontrada.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
