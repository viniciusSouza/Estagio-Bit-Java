package com.company.lista4;

public abstract class ContaBancaria implements Imprimivel, Comparable<ContaBancaria>{
    protected int numeroConta;
    protected double saldo;
    protected String titular;

    public double getSaldo() {
        return saldo;
    }

    public int getNumeroConta() {
        return numeroConta;
    }

    public String getTitular() {
        return titular;
    }

    public abstract boolean sacar(double valor);

    public abstract boolean  depositar(double valor);

    public  abstract void transferir(double valor, ContaBancaria contaBancaria);

    public int compareTo(ContaBancaria contaBancaria) {
        return this.titular.compareTo(contaBancaria.titular);
    }
}
