package com.company.lista4;

import com.company.lista5.SaldoInsuficienteException;

public class ContaCorrente extends ContaBancaria {
    private double taxaDeOperacao;

    public ContaCorrente(int numeroConta, double saldo, double taxaDeOperacao, String titular) {
        this.numeroConta = numeroConta;
        this.saldo = saldo;
        this.taxaDeOperacao = taxaDeOperacao;
        this.titular = titular;
    }

    @Override
    public boolean sacar(double valor) {
        try {
            if (valor < 0) {
                throw new IllegalArgumentException("O valor de saque não pode ser negativo.");
            }
            if (valor <= this.saldo - this.taxaDeOperacao) {
                this.saldo -= valor + this.taxaDeOperacao;
                return true;
            } else {
                throw new SaldoInsuficienteException("Saldo insuficiente para realizar o saque de: ",valor);
            }
        } catch (IllegalArgumentException | SaldoInsuficienteException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean depositar(double valor) {
        try {
            if (valor > 0) {
                this.saldo += valor - this.taxaDeOperacao;
                return true;
            } else {
                throw new IllegalArgumentException("O valor de depósito deve ser maior que 0.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public void transferir(double valor, ContaBancaria contaBancaria) {
        try {
            if (this.sacar(valor)) {
                contaBancaria.depositar(valor);
            } else {
                throw new SaldoInsuficienteException("Saldo insuficiente para realizar trânsferência de: ", valor);
            }
        } catch (SaldoInsuficienteException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void mostrarDados() {
        System.out.println("Titular: " + this.titular + " Numero da conta: " + this.numeroConta +
                            " Saldo: " + this.saldo + " Taxa de operacao: " +
                            this.taxaDeOperacao);
    }
}
