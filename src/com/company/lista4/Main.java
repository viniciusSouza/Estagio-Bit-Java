package com.company.lista4;

public class Main {
    public static void main(String[] args) {
        ContaCorrente cc = new ContaCorrente(123, 500., 2., "Joao");
        ContaPoupanca cp = new ContaPoupanca(1234, 500., 100., "Maria");
        Relatorio relatorio = new Relatorio();
        relatorio.gerarRelatorio(cc);
        relatorio.gerarRelatorio(cp);
        cc.depositar(-100.);
        cp.depositar(-200.);
        cc.sacar(500.);
        cp.sacar(500.);
        relatorio.gerarRelatorio(cc);
        relatorio.gerarRelatorio(cp);
        cc.sacar(400.);
        cp.depositar(1000.);
        cc.depositar(100.);
        relatorio.gerarRelatorio(cc);
        relatorio.gerarRelatorio(cp);

        cc.transferir(20., cp);
        relatorio.gerarRelatorio(cc);
        relatorio.gerarRelatorio(cp);

        cp.transferir(101., cc);
        relatorio.gerarRelatorio(cc);
        relatorio.gerarRelatorio(cp);

        cc.sacar(-200.);
        cp.sacar(-200.);
        cp.sacar(1500.);

        cp.transferir(2000., cc);
        cc.transferir(2000., cp);
    }
}
