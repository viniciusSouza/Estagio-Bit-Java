package com.company.lista4;

import com.company.lista5.SaldoInsuficienteException;

public class ContaPoupanca extends ContaBancaria {
    private double limite;

    public ContaPoupanca(int numeroConta, double saldo, double limite, String titular) {
        this.numeroConta = numeroConta;
        this.saldo = saldo;
        this.limite = limite;
        this.titular = titular;
    }

    @Override
    public boolean sacar(double valor) {
        try {
            if (valor < 0) {
                throw new IllegalArgumentException("O valor de saque não pode ser negativo.");
            }
            if (valor <= this.saldo + this.limite) {
                this.saldo -= valor;
                return true;
            } else {
                throw new SaldoInsuficienteException("Saldo insuficiente para sacar o valor de: ",valor);
            }
        } catch (IllegalArgumentException | SaldoInsuficienteException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean depositar(double valor) {
        try {
            if (valor > 0) {
                this.saldo += valor;
                return true;
            } else {
                throw new IllegalArgumentException("O valor de depósito deve ser maior que 0.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public void transferir(double valor, ContaBancaria contaBancaria) {
        try {
            if (this.sacar(valor)) {
                contaBancaria.depositar(valor);
            } else {
                throw new SaldoInsuficienteException("Saldo insuficiente para realizar transferência de valor: ", valor);
            }
        } catch (SaldoInsuficienteException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void mostrarDados() {
        System.out.println("Titular: " + this.titular +" Numero da conta: " + this.numeroConta +
                           " Saldo: " + this.saldo + " Limite: " +
                           this.limite);
    }
}
