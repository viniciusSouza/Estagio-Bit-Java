package com.company.lista4;

public class TesteBanco {
    public static void main(String[] args) {
        ContaPoupanca cp = new ContaPoupanca(123, 500., 100., "Vinicius");
        ContaCorrente cc = new ContaCorrente(1234, 1000., 2., "Joao");
        Banco banco = new Banco();
        banco.inserirConta(null);
        banco.inserirConta(cc);
        banco.inserirConta(cp);
        ContaBancaria cb = banco.procurarConta(123);
        cb.mostrarDados();
        cb = banco.procurarConta(1234);
        cb.mostrarDados();
        banco.mostrarDados();
        banco.removerConta(cp);
        banco.procurarConta(123);
        banco.mostrarDados();
    }
}
