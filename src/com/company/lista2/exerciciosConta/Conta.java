package com.company.lista2.exerciciosConta;

public class Conta {
    private static int totalContas = 1;
    private int id;
    private String nome;
    private int numero;
    private String agencia;
    private double saldo;
    private Data dataAbertura;

    public Conta(String nome, Integer numero, String agencia, Double saldo, Data dataAbertura) {
        this.nome = nome;
        this.numero = numero;
        this.agencia = agencia;
        this.saldo = saldo;
        this.dataAbertura = dataAbertura;
        this.id = totalContas++;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNumero() {
        return numero;
    }

    public String getAgencia() {
        return agencia;
    }

    public double getSaldo() {
        return saldo;
    }

    public String getDataAbertura() {
        return dataAbertura.imprimeData();
    }

    public void saca(Double valor) {
        if (this.saldo >= valor) {
            this.saldo -= valor;
        } else {
            System.out.println("Saldo insuficiente.");
        }
    }

    public void deposita(Double valor) {
        this.saldo += valor;
    }

    public double calculaRendimento() {
        return this.saldo*0.1;
    }

    public String recuperaDadosParaImpressao() {
        return ("Nome: " + getNome() + "; Numero: " + getNumero() +
                "; Agencia: " + getAgencia() + "; Saldo: " + getSaldo() +
                "; Data: " + getDataAbertura());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Conta && ((Conta) obj).getNumero() == this.numero && ((Conta) obj).getAgencia().equals(this.agencia)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.agencia.hashCode();
    }

    @Override
    public String toString() {
        return ("Nome: " + getNome() + "; Numero: " + getNumero() +
                "; Agencia: " + getAgencia() + "; Saldo: " + getSaldo() +
                "; Data: " + getDataAbertura());
    }
}
