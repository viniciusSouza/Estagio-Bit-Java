package com.company.lista2.exerciciosConta;

import javax.xml.bind.ValidationException;

public class TesteConta {

    public static void main(String[] args) {
        //Exercícios da relacionados a Conta

        try {
            Data d1 = new Data(13,4,1992);
            Conta c1 = new Conta("Vinicius", 1234, "Centro", 567d, d1);
            c1.saca(600d);
            c1.saca(500d);
            System.out.println(c1.getSaldo());
            c1.deposita(100d);
            System.out.println(c1.getSaldo());
            System.out.println(c1.calculaRendimento());
            System.out.println(c1.recuperaDadosParaImpressao());

            // Lista 2B - Exercício 5

            System.out.println(c1.getId());
            Conta c2 = new Conta("Joao",12345,"Maracana",765d, d1);
            Conta c3 = new Conta("Maria", 123, "Centro", 321d, d1);
            System.out.println(c2.getId());
            System.out.println(c3.getId());
            System.out.println(c1.getId());
            Conta c4 = new Conta("Maria", 123, "Centro", 321d, d1);
            System.out.println(c4.getId());

            //Lista 2B - Exercício 6

            Data d2 = new Data(30,3,2000);
            Data d3 = new Data(31,1,1954);
            Conta c5 = new Conta("Joao", 123,"Centro",123d,d3);
            System.out.println(c5.getNome());
            Data d4 = new Data(35,1,1954);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
