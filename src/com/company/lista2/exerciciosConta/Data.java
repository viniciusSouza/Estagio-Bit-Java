package com.company.lista2.exerciciosConta;

import javax.xml.bind.ValidationException;

public class Data {
    private int dia;
    private int mes;
    private int ano;

    public Data(int dia, int mes, int ano) throws Exception {
        if (validaData(dia,mes,ano)) {
            this.dia = dia;
            this.mes = mes;
            this.ano = ano;
        } else {
            throw new Exception("Data " + dia + "/" + mes + "/"
                                + ano + " é inválida.");
        }
    }

    public Boolean validaData(int dia, int mes,int ano) {
        Boolean validator = true;
        if (ano >= 1900) {
            switch (mes) {
                case 1:
                    if (dia > 31 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 2:
                    if (dia > 28 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 3:
                    if (dia > 31 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 4:
                    if (dia > 30 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 5:
                    if (dia > 31 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 6:
                    if (dia > 30 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 7:
                    if (dia > 31 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 8:
                    if (dia > 31 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 9:
                    if (dia > 30 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 10:
                    if (dia > 31 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 11:
                    if (dia > 30 || dia < 0) {
                        validator = false;
                    }
                    break;
                case 12:
                    if (dia > 31 || dia < 0) {
                        validator = false;
                    }
                    break;
            }
        }
        return validator;
    }

    public String imprimeData() {
        return (this.dia + "/" + this.mes + "/" + this.ano);
    }
}
