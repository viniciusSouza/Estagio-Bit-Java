package com.company.lista2;

public class Porta {
    private Double dimensaoX;
    private Double dimensaoY;
    private Double dimensaoZ;
    private Boolean aberta;
    private String cor;

    public Porta(Double dimensaoX, Double dimensaoY, Double dimensaoZ, Boolean aberta, String cor) {
        this.dimensaoX = dimensaoX;
        this.dimensaoY = dimensaoY;
        this.dimensaoZ = dimensaoZ;
        this.aberta = aberta;
        this.cor = cor;
    }

    public Double getDimensaoX() {
        return dimensaoX;
    }

    public void setDimensaoX(Double dimensaoX) {
        this.dimensaoX = dimensaoX;
    }

    public Double getDimensaoY() {
        return dimensaoY;
    }

    public void setDimensaoY(Double dimensaoY) {
        this.dimensaoY = dimensaoY;
    }

    public Double getDimensaoZ() {
        return dimensaoZ;
    }

    public void setDimensaoZ(Double dimensaoZ) {
        this.dimensaoZ = dimensaoZ;
    }

    public String getCor() {
        return cor;
    }

    public void abreFechaPorta() {
        aberta = !aberta;
    }

    public Boolean isOpen() {
        return aberta;
    }

    public void pintaPorta(String cor) {
        if (this.cor == cor) {
            System.out.println("A porta já é desta cor!");
        } else {
            this.cor = cor;
        }
    }
}
