package com.company.lista2;

public class FabricaDeCarro {
    private static FabricaDeCarro instancia;
    private String nome;

    private FabricaDeCarro(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public static FabricaDeCarro getInstancia(String nome) {
        if (instancia == null) {
            instancia = new FabricaDeCarro(nome);
        }
        return instancia;
    }
}
