package com.company.lista2;

public class Casa {
    private String cor;
    private Porta p1;
    private Porta p2;
    private Porta p3;

    public Casa(String cor, Porta p1, Porta p2, Porta p3) {
        this.cor = cor;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public void pintaCasa(String cor) {
        if (this.cor == cor) {
            System.out.println("A casa já é dessa cor!");
        } else {
            this.cor = cor;
        }
    }

    public int quantidadePortasAbertas() {
        int total = 0;
        if (p1.isOpen()) {
            total++;
        }
        if (p2.isOpen()) {
            total++;
        }
        if (p3.isOpen()) {
            total++;
        }
        return total;
    }
}
