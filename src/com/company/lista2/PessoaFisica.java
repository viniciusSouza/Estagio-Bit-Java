package com.company.lista2;

public class PessoaFisica {
    private String nome;
    private Integer cpf;

    public PessoaFisica(String nome, Integer cpf, int teste) throws Exception {
        if (validaCpf(cpf, teste)) {
            this.nome = nome;
            this.cpf = cpf;
        } else {
            throw new Exception("CPF: " + cpf + " de " + nome + " é inválido.");
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCpf() {
        return cpf;
    }

    public Boolean validaCpf(int cpf, int testeCpf) {
        Boolean validator;
        if (testeCpf == 1) {
            validator = true;
            return validator;
        } else {
            validator = false;
            return validator;
        }
    }
}
