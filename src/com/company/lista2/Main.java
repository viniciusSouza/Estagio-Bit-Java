package com.company.lista2;

public class Main {

    public static void main(String[] args) {
        //Lista 2A - Desafios Fibonacci

        int limite = 8;
        for (int i = 1; i <= limite; i++) {
            int fibo = Fibonacci.fibonacciRecursivo(i);
            System.out.print(fibo + " ");
        }
        System.out.println();
        for (int i = 1; i <= limite; i++) {
            int fibo = Fibonacci.fibonacciUmaLinha(i);
            System.out.print(fibo + " ");
        }
        System.out.println();

        //Lista 2A - Programa 1

        Pessoa pessoa = new Pessoa("Vinicius", 25);
        System.out.println(pessoa.getNome() + " tem: " + pessoa.getIdade());
        for (int i = 1; i <= 4; i++) {
            pessoa.fazAniversario();
        }
        System.out.println(pessoa.getNome() + " tem: " + pessoa.getIdade());

        //Lista 2A - Programa 2

        Porta porta = new Porta(3d,4d,5d,false,"Verde");
        porta.abreFechaPorta();
        porta.abreFechaPorta();
        porta.abreFechaPorta();
        porta.abreFechaPorta();
        Boolean aberta = porta.isOpen();
        if (aberta) {
            System.out.println("Porta aberta.");
        } else {
            System.out.println("Porta fechada.");
        }
        System.out.println("Cor original da porta: " + porta.getCor());
        porta.pintaPorta("Verde");
        porta.pintaPorta("Azul");
        System.out.println("Cor nova da porta: " + porta.getCor());
        System.out.println("Dimensões originais da porta: " + porta.getDimensaoX() + ", " +
                            porta.getDimensaoY() + ", " + porta.getDimensaoZ());
        porta.setDimensaoX(7d);
        porta.setDimensaoY(9d);
        porta.setDimensaoZ(10d);
        System.out.println("Dimensões novas da porta: " + porta.getDimensaoX() + ", " +
                            porta.getDimensaoY() + ", " + porta.getDimensaoZ());

        //Lista 2A - Programa 3

        Porta p1 = new Porta(3d,3d,3d,false,"Verde");
        Porta p2 = new Porta(3d,3d,3d,true,"Azul");
        Porta p3 = new Porta(3d,3d,3d,false,"Amarelo");

        Casa casa = new Casa("Rosa", p1,p2,p3);
        System.out.println("Total de portas abertas: " + casa.quantidadePortasAbertas());
        p1.abreFechaPorta();
        System.out.println("Total de portas abertas: " + casa.quantidadePortasAbertas());
        p3.abreFechaPorta();
        System.out.println("Total de portas abertas: " + casa.quantidadePortasAbertas());
        p2.abreFechaPorta();
        System.out.println("Total de portas abertas: " + casa.quantidadePortasAbertas());

        //Lista 2B - 7

        try {
            PessoaFisica pf1 = new PessoaFisica("Joao", 12345, 1);
            System.out.println(pf1.getNome() + " " + pf1.getCpf());
            PessoaFisica pf2 = new PessoaFisica("Maria", 1234578, 0);
            System.out.println(pf2.getNome());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //Desafio - Singleton

        FabricaDeCarro fc = FabricaDeCarro.getInstancia("Fabrica 1");
        System.out.println(fc.getNome());
        FabricaDeCarro fc2 = FabricaDeCarro.getInstancia("Fabrica 2");
        System.out.println(fc2.getNome());

    }
}
