package com.company.lista2;

public class Fibonacci {

    public static Integer fibonacciRecursivo(int numero) {
        if (numero <= 1) {
            return numero;
        } else {
            return fibonacciRecursivo(numero-1) + fibonacciRecursivo(numero - 2);
        }
    }

    public static Integer fibonacciUmaLinha(int numero) {
        return (numero <= 1) ? numero : fibonacciUmaLinha(numero -1) + fibonacciUmaLinha(numero -2);
    }
}
