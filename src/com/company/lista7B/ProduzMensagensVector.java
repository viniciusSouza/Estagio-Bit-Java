package com.company.lista7B;

import java.util.Collection;

public class ProduzMensagensVector implements Runnable {
    private int comeco;
    private int fim;
    private Collection<String> mensagens;

    public ProduzMensagensVector(int comeco, int fim, Collection<String> mensagens) {
        this.comeco = comeco;
        this.fim = fim;
        this.mensagens = mensagens;
    }

    @Override
    public void run() {
        for (int i = comeco; i < fim; i++) {
            mensagens.add("Mensagem: " + i);
        }
    }
}
