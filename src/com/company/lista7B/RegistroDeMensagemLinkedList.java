package com.company.lista7B;

import java.util.Collection;
import java.util.LinkedList;

public class RegistroDeMensagemLinkedList {
    public static void main(String[] args) throws InterruptedException {
        Collection<String> mensagens = new LinkedList<>();

        Thread t1 = new Thread(new ProduzMensagemLinkedList(0,10_000, mensagens));
        Thread t2 = new Thread(new ProduzMensagemLinkedList(10_000,20_000, mensagens));
        Thread t3 = new Thread(new ProduzMensagemLinkedList(20_0000,30_000, mensagens));

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        System.out.println("Threads produtoras finalizadas.");

        for (int i = 0; i < 15000; i++) {
            if (!mensagens.contains("Mensagem: " + i)) {
                throw new IllegalStateException("Não encontrei a mensagem: " + i);
            }
        }

        if (mensagens.contains(null)) {
            throw new IllegalStateException("Não devia ter null aqui dentro!");
        }

        System.out.println("Fim da execucao com sucesso");
    }
}
