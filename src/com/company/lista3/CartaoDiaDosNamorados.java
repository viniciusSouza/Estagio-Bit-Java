package com.company.lista3;

public class CartaoDiaDosNamorados extends CartaoWeb {
    public CartaoDiaDosNamorados(String nome) {
        this.destinatario = nome;
    }

    @Override
    public String showMessage() {
        return "Feliz dia dos namorados " + this.destinatario;
    }
}
