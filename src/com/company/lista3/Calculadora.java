package com.company.lista3;

public class Calculadora {
    public double addOperation(double number1, double number2) {
        return number1 + number2;
    }

    public double minusOperation(double number1, double number2) {
        return number1 - number2;
    }

    public double divisionOperation(double number1, double number2) {
        if (number2 == 0) {
            throw new IllegalArgumentException("Impossível realizar divisão por 0.");
        } else {
            return number1/number2;
        }
    }

    public double multiplicationOperation(double number1, double number2) {
        return number1*number2;
    }
}
