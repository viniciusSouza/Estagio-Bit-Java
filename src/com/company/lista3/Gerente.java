package com.company.lista3;

public class Gerente extends Funcionario {
    public Gerente(String nome, int matricula, double salario) {
        this.nome = nome;
        this.matricula = matricula;
        this.salario = salario;
    }

    @Override
    public double calculaSalario() {
        return salario*2;
    }
}
