package com.company.lista3;

public class CartaoNatal extends CartaoWeb {
    public CartaoNatal(String nome) {
        this.destinatario = nome;
    }

    @Override
    public String showMessage() {
        return "Feliz natal " + this.destinatario;
    }
}
