package com.company.lista3;

public class CartaoAniversario extends CartaoWeb {
    public CartaoAniversario(String nome) {
        this.destinatario = nome;
    }

    @Override
    public String showMessage() {
        return "Feliz aniversário " + this.destinatario;
    }
}
