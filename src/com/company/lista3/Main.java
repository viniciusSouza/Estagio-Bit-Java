package com.company.lista3;

public class Main {
    public static void main(String[] args) {
        /* Lista 3 - Exercício 1 -
        Calculadora calc = new Calculadora();
        CalculadoraCientifica calcCi = new CalculadoraCientifica();
        System.out.println(calc.addOperation(1.,2.));
        try {
            System.out.println(calc.divisionOperation(4.,2.));
            System.out.println(calc.divisionOperation(2.,0));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(calc.multiplicationOperation(2.,2.));
        System.out.println(calc.minusOperation(2.,2.));
        System.out.println(calcCi.exponencialOperation(2.,2.));
        try {
            System.out.println(calcCi.squareOperation(4.));
            System.out.println(calcCi.squareOperation(-2.));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        */

        /* Lista 3 - Exercicio 2 -
        Assistente assistente = new Assistente("Vinicius", 123, 1000.);
        Vendedor vendedor = new Vendedor("Joao", 1234, 1000., 500.);
        Gerente gerente = new Gerente("Maria", 12345, 1000.);
        double valorTotal = gerente.calculaSalario() + vendedor.calculaSalario() + assistente.calculaSalario();
        System.out.println(valorTotal);
        */

        /* Lista 3 - Exercício 3 -
        CartaoAniversario cartaoAniversario = new CartaoAniversario("Vinicius");
        CartaoDiaDosNamorados cartaoDiaDosNamorados = new CartaoDiaDosNamorados("Maria");
        CartaoNatal cartaoNatal = new CartaoNatal("Joao");
        CartaoWeb[] cartoes = new CartaoWeb[3];
        cartoes[0] = cartaoAniversario;
        cartoes[1] = cartaoNatal;
        cartoes[2] = cartaoDiaDosNamorados;
        for (CartaoWeb c : cartoes) {
            System.out.println(c.showMessage());
        }
        */
    }
}
