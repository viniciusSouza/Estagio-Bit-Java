package com.company.lista3;

public abstract class Funcionario {
    protected String nome;
    protected int matricula;
    protected double salario;

    public String getNome() {
        return nome;
    }

    public int getMatricula() {
        return matricula;
    }

    public abstract double calculaSalario();
}
