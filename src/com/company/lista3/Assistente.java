package com.company.lista3;

public class Assistente extends Funcionario {
    public Assistente(String nome, int matricula, double salario) {
        this.nome = nome;
        this.matricula = matricula;
        this.salario = salario;
    }

    @Override
    public double calculaSalario() {
        return salario;
    }
}
