package com.company.lista3;

public class Vendedor extends Funcionario {
    private double comissao;

    public Vendedor(String nome, int matricula, double salario, double comissao) {
        this.nome = nome;
        this.matricula = matricula;
        this.salario = salario;
        this.comissao = comissao;
    }

    @Override
    public double calculaSalario() {
        return salario + this.comissao;
    }
}
