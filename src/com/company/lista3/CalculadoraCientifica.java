package com.company.lista3;

public class CalculadoraCientifica extends Calculadora {
    public double squareOperation(double number) {
        if (number <= 0) {
            throw new IllegalArgumentException("Impossível calcular a raiz de um número negativo.");
        } else {
            return Math.sqrt(number);
        }
    }

    public double exponencialOperation(double number, double expoent) {
        return Math.pow(number,expoent);
    }
}
