package com.company.lista6C;

import com.company.lista2.exerciciosConta.Conta;
import com.company.lista2.exerciciosConta.Data;

public class Main {
    public static void main(String[] args) {
        try {
            Data d1 = new Data(29, 4, 2018);
            Conta conta1 = new Conta("Joao", 123, "Centro", 200., d1);
            Conta conta2 = new Conta("Maria", 1234, "Centro", 200., d1);
            Conta conta3 = new Conta("Vinicius", 123, "Centro", 200., d1);

            System.out.println(conta1.toString());
            System.out.println(conta1.equals(conta2));
            System.out.println(conta1.equals(conta3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
