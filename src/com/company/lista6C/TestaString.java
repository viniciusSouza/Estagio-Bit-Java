package com.company.lista6C;

public class TestaString {
    public static void main(String[] args) {
        String s = "fj11";
        s = s.replaceAll("1","2");
        System.out.println(s);

        String s2 = "fj";
        System.out.println(s.contains(s2));

        String s3 = "                teste          ";
        System.out.println(s3);
        s3 = s3.trim();
        System.out.println(s3);

        System.out.println(s3.isEmpty());

        System.out.println(s3.length());

        TestaString.imprimeLetrasString("Hello World");
        TestaString.imprimeStringTrasPraFrente("Socorram-me, subi no ônibus em Marrocos");
        System.out.println();
        TestaString.imprimeStringTrasPraFrente("anotaram a data da maratona");
        System.out.println();
        System.out.println(TestaString.imprimeStringTrasPraFrenteComStringBuilder("Socorram-me, subi no ônibus em Marrocos"));
        int numero1 = (TestaString.converteStringParaInt("180"));
        int numero2 = (TestaString.converteStringParaInt("200"));
        System.out.println(numero1 + numero2);
    }

    public static void imprimeLetrasString(String s) {
        for (int i = 0; i < s.length(); i++) {
            System.out.println(s.charAt(i));
        }
    }

    public  static void imprimeStringTrasPraFrente(String s) {
        for (int i = s.length() - 1; i >= 0; i--) {
            System.out.print(s.charAt(i));
        }
    }

    public  static String imprimeStringTrasPraFrenteComStringBuilder(String s) {
        StringBuilder sb = new StringBuilder(s);
        sb.reverse();
        return sb.toString();
    }

    public static int converteStringParaInt(String numero) {
        char[] algorismos = numero.toCharArray();
        int casaDecimal = algorismos.length-1;
        int num = 0;
        for (char c : algorismos) {
            num += (c - '0')*(Math.pow(10,casaDecimal));
            casaDecimal--;
        }
        return num;
    }
}
