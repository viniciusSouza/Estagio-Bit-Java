package com.company.lista6B;

import com.company.lista4.Banco;
import com.company.lista4.ContaBancaria;
import com.company.lista4.ContaCorrente;

public class TesteBanco {

    public static void main(String[] args) {
        Banco banco = new Banco();
        int total = 500000;
        for (int i = 0; i < total; i++) {
            ContaCorrente cc = new ContaCorrente(i, i * 10., 2., "Joao" + i);
            banco.inserirConta(cc);
        }
        System.out.println("Total de contas: " + banco.totalDeContas());
        String nome = "Joao" + 250000;
        long inicio = System.currentTimeMillis();
        ContaBancaria cb = banco.buscarContaPorTitular(nome);
        long tempo = System.currentTimeMillis() - inicio;
        System.out.println("Tempo de busca da conta na List: " + tempo);
        if (cb != null) {
            System.out.println("Conta encontrada: " + cb.getTitular());
        }
        System.out.println();
        inicio = System.currentTimeMillis();
        cb = banco.buscarContaPorTitularNoMap(nome);
        tempo = System.currentTimeMillis() - inicio;
        System.out.println("Tempo de busca da conta no Map: " + tempo);
        if (cb != null) {
            System.out.println("Conta encontrada: " + cb.getTitular());

        }
    }
}
