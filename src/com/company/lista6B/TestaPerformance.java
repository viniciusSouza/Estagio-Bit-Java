package com.company.lista6B;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class TestaPerformance {
    public static void main(String[] args) {
        long incio = System.currentTimeMillis();
        Collection<Integer> testeArrayList = new ArrayList<>();
        int total = 50000;
        for (int i = 0; i < total; i++) {
            testeArrayList.add(i);
        }
        long tempo = System.currentTimeMillis() - incio;
        System.out.println("Tempo gasto na inserção ArrayList: " + tempo);
        incio = System.currentTimeMillis();
        for (Integer i : testeArrayList) {
            testeArrayList.contains(i);
        }
        tempo = System.currentTimeMillis() - incio;
        System.out.println("Tempo gasto na busca ArrayList: " + tempo);

        System.out.println();
        incio = System.currentTimeMillis();
        Collection<Integer> testeHash = new HashSet<>();
        for (int i = 0; i < total; i++) {
            testeHash.add(i);
        }
        tempo = System.currentTimeMillis() - incio;
        System.out.println("Tempo gasto na inserção Hash: " + tempo);
        incio = System.currentTimeMillis();
        for (Integer i : testeHash) {
            testeHash.contains(i);
        }
        tempo = System.currentTimeMillis() - incio;
        System.out.println("Tempo gasto na busca Hash: " + tempo);
    }
}
