package com.company.lista6B;

import com.company.lista2.exerciciosConta.Conta;
import com.company.lista2.exerciciosConta.Data;

import java.util.HashSet;
import java.util.Set;

public class TesteHashCode {
    public static void main(String[] args) {
        try {
            Data d1 = new Data(29,4,2018);
            Conta c1 = new Conta("Joao", 222, "Centro", 600., d1);
            Conta c2 = new Conta("Vinicius", 123, "Centro", 700., d1);
            Conta c3 = new Conta("Maria", 222, "Centro", 700., d1);
            Conta c4 = new Conta("Bob", 123, "Centro", 1000., d1);
            Conta c5 = new Conta("Ronaldo", 123, "Centro", 1000., d1);
            Conta c6 = new Conta("Neymar", 12345, "Centro", 1000., d1);
            Conta c7 = new Conta("Neymar", 12345, "Tijuca", 1000., d1);
            Conta c8 = new Conta("Coutinho", 12345, "Tijuca", 1000., d1);
            Set<Conta> contasSet = new HashSet<>();
            contasSet.add(c1);
            contasSet.add(c2);
            contasSet.add(c3);
            contasSet.add(c4);
            contasSet.add(c5);
            contasSet.add(c6);
            contasSet.add(c7);
            contasSet.add(c8);
            System.out.println("Tamanho do Set: " + contasSet.size());
            for (Conta c : contasSet) {
                System.out.println(c.recuperaDadosParaImpressao());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
