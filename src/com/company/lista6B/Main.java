package com.company.lista6B;

import com.company.lista4.Banco;
import com.company.lista4.ContaCorrente;
import com.company.lista4.ContaPoupanca;
import java.util.Collections;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        ContaCorrente cc = new ContaCorrente(123, 500., 2., "Joao");
        ContaPoupanca cp = new ContaPoupanca(123, 500., 100., "Vinicius");
        int compare = cp.compareTo(cp);
        System.out.println(compare);
        compare = cp.compareTo(cc);
        System.out.println(compare);
        compare = cc.compareTo(cp);
        System.out.println(compare);

        Banco banco = new Banco();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            ContaCorrente conta = new ContaCorrente(i,random.nextInt(40)*100, 2., "Joao" + i);
            banco.inserirConta(conta);
        }
        banco.mostrarDados();
        Collections.reverse(banco.getContasBancarias());
        System.out.println();
        banco.mostrarDados();
        System.out.println();
        Collections.shuffle(banco.getContasBancarias());
        banco.mostrarDados();
        System.out.println();
        Collections.rotate(banco.getContasBancarias(), 2);
        banco.mostrarDados();
    }
}
