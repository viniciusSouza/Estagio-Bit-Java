package com.company.lista6B;

import com.company.lista2.exerciciosConta.Conta;
import com.company.lista4.ContaBancaria;
import com.company.lista4.ContaCorrente;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class TestaLista {
    public static void main(String[] args) {
        List<ContaBancaria> contas = new LinkedList<>();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            ContaCorrente conta = new ContaCorrente(i, random.nextInt(50) * 100, 2., "Joao" + i);
            contas.add(conta);
        }
        //Poderia reescrever o método toString de ArrayList criando uma classe que estendesse dela e dando Override no método.
        System.out.println(contas);
    }
}
