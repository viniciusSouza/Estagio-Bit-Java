package com.company.lista6B;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestePerformanceDeAdicionarNaPrimeiraPosicao {
    public static void main(String[] args) {
        List<Integer> testeLinkedList = new LinkedList<>();
        int total = 50000;
        long inicio = System.currentTimeMillis();
        for (int i = 0; i < total; i++) {
            testeLinkedList.add(0,i);
        }
        long tempo = System.currentTimeMillis() - inicio;
        System.out.println("Tempo para inserção LinkedList: " + tempo);
        inicio = System.currentTimeMillis();
        Integer numero = testeLinkedList.get(49000);
        tempo = System.currentTimeMillis() - inicio;
        System.out.println("Tempo usando o get(index) LinkedList: " + tempo);
        System.out.println();

        List<Integer> testeArrayList = new ArrayList<>();
        inicio = System.currentTimeMillis();
        for (int i = 0; i < total; i++) {
            testeArrayList.add(0,i);
        }
        tempo = System.currentTimeMillis() - inicio;
        System.out.println("Tempo para inserção ArrayList: " + tempo);
        inicio = System.currentTimeMillis();
        numero = testeArrayList.get(49000);
        tempo = System.currentTimeMillis() - inicio;
        System.out.println("Tempo usando o get(index) ArayList: " + tempo);
    }
}
