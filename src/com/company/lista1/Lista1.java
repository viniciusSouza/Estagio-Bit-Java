package com.company.lista1;

public class Lista1 {

    public static void listaNumeros(int limiteInf, int limiteSup) {
        for (int i = limiteInf; i <= limiteSup; i++) {
            System.out.println(i);
        }
    }

    public static void somaTodosNumeros(int limiteInf, int limiteSup) {
        int soma = 0;
        for (int i = limiteInf; i<=limiteSup; i++) {
            soma += i;
        }
        System.out.println("A soma de " + limiteInf + " até " +
                            limiteSup + " é: " + soma);
    }

    public static void multiplosDeNumero(int numero, int limiteInf, int limiteSup) {
        System.out.println("A lista de múltiplos de " + numero +
                            " entre " + limiteInf + " e " + limiteSup +
                            " é:");
        for (int i = limiteInf; i<=limiteSup; i++) {
            if (i%3 == 0) {
                System.out.print(i + " ");
            }
        }
    }

    public static void fatorialLimiteDefinido(int numero) {
        int fatorial = 1;
        System.out.println("O fatorial de 0 é: " + fatorial);
        for (int i = 1; i<= numero; i++) {
            fatorial = fatorial * i;
            System.out.println("O fatorial de " + i + " é: " + fatorial);
        }
    }

    public static void fibonacciComLimite(int limite) {
        int fibonacci = 0;
        int n1 = 0;
        int n2 = 1;
        System.out.print("Fibonacci: 0, 1, ");
        while (fibonacci <= limite) {
            fibonacci = n1 + n2;
            n1 = n2;
            n2 = fibonacci;
            if (fibonacci < limite) {
                System.out.print(fibonacci + ", ");
            } else {
                System.out.print(fibonacci + ".");
            }
        }
    }

    public static void aplicacaoDeRegra(int numero) {
        while (numero != 1) {
            if (numero % 2 == 0) {
                numero = numero / 2;
            } else {
                numero = (numero * 3) + 1;
            }
            if (numero == 1) {
                System.out.print(numero + ".");
            } else {
                System.out.print(numero + ", ");
            }
        }
    }

    public static void tabela(int limite) {
        for (int i = 1; i<= limite; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j*i + " ");
            }
            System.out.println();
        }
    }

    public static void fibonacciDesafio(int limite) {
        int n1 = 0;
        int fibonacci = 1;
        System.out.print("Fibonacci: 0, 1, ");
        while (fibonacci <= limite) {
            fibonacci = fibonacci + n1;
            n1 = fibonacci - n1;
            if (fibonacci < limite) {
                System.out.print(fibonacci + ", ");
            } else {
                System.out.print(fibonacci + ".");
            }
        }
    }
}
