package com.company.lista1;

public class Main {

    public static void main(String[] args) {
        Lista1.listaNumeros(150,300);
        Lista1.somaTodosNumeros(1,1000);
        Lista1.multiplosDeNumero(3,1,100);
        System.out.println();
        Lista1.fatorialLimiteDefinido(10);
        Lista1.fibonacciComLimite(100);
        System.out.println();
        Lista1.aplicacaoDeRegra(13);
        System.out.println();
        Lista1.tabela(4);
        Lista1.fibonacciDesafio(100);
    }
}
